## Previous submission: SDraw 2.1.2
* Expanded description to describe acronyms, as suggested by Hornig. -Thank you.
* Removed dependencies on OpenStreetMap and rgdal

## Test environments
* local Windows 10 Pro, R 3.3.0
* passes devtools::build_win()
* Linux (via travis)

## R CMD check --as-cran results
*No ERRORs.  
*No WARNINGs: 

## Downstream dependencies
None known
